#!/bin/bash

cp -r ~/RandomWordAPI ~/backup/RandomWordAPI

rm -rf ~/RandomWordAPI

cd ~/ || exit

# clone the repo again
git clone https://github.com/failing/RandomWordAPI.git

cp ~/config.py ~/RandomWordAPI/app/config.py

cd RandomWordAPI/ || exit

sudo docker ps | grep ubuntu:latest | awk '{print $1}' | xargs sudo docker stop

sudo docker ps | grep mongo:4.2.1-bionic | awk '{print $1}' | xargs sudo docker stop

sudo docker-compose up -d --build