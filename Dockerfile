FROM python:3.7-stretch

ARG requirements
RUN apt-get update

RUN mkdir application

COPY app/ /application
WORKDIR /application
RUN pip install -r requirements.txt

EXPOSE 5000