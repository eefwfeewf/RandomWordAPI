from flask import *
from flask_pymongo import PyMongo
from bson import ObjectId, json_util
import json
from flask_jwt_simple import (
    JWTManager, jwt_required, create_jwt, get_jwt_identity
)

app = Flask(__name__)
app.config["MONGO_URI"] = "mongodb://mongo:27017/wordsDB"
app.config.from_object('config')

jwt = JWTManager(app)
mongo = PyMongo(app)


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


@app.route('/api/v1/auth', methods=['POST', 'GET'])
def login():
    if request.method == "GET":
        return jsonify({"msg": "Missing JSON in request"}), 404
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    if username != app.config["JWT_USER"] or password != app.config['JWT_PASS']:
        return jsonify({"msg": "Bad username or password"}), 401

    ret = {'jwt': create_jwt(identity=username)}
    return jsonify(ret), 200


@app.route("/api/v1/single/less/<int:length>", methods=['GET'])
def single_less(length):
    word = {
        "status": "success",
    }

    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$lt": [{"$strLenCP": "$word"}, length]}}},
            {"$sample": {"size": 1}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/single/greater/<int:length>", methods=['GET'])
def single_greater(length):
    word = {
        "status": "success",
    }
    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$gt": [{"$strLenCP": "$word"}, length]}}},
            {"$sample": {"size": 1}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/multiple/<int:amount>/equal/<int:length>", methods=['GET'])
def multiple_equal(amount, length):
    word = {
        "status": "success",
    }
    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$eq": [{"$strLenCP": "$word"}, length]}}},
            {"$sample": {"size": amount}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/multiple/<int:amount>/more/<int:length>", methods=['GET'])
def multiple_greater(amount, length):
    word = {
        "status": "success",
    }

    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$gt": [{"$strLenCP": "$word"}, length]}}},
            {"$sample": {"size": amount}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/multiple/<int:amount>/less/<int:length>", methods=['GET'])
def multiple_less(amount, length):
    word = {
        "status": "success",
    }
    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$lt": [{"$strLenCP": "$word"}, length]}}},
            {"$sample": {"size": amount}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/chain/<int:amount>", methods=['GET'])
def chain(amount):
    chain = get_random_doc([
        {"$sample": {"size": amount}},
        {"$unset": "_id"},
    ])

    chain_ret = ""
    for a in chain['words']:
        chain_ret += a

    return {
        "status": "success",
        "word_chain": chain_ret
    }


@app.route("/api/v1/add", methods=["POST"])
@jwt_required
def addword():
    word = None

    try:
        word = request.json
    except Exception:
        return {
            "status": "failed",
            "reason": "invalid json"
        }

    new_word = {
        "word": word['word']
    }

    if mongo.db.words.count({"word": f"{word['word']}"}) != 0:
        return {
            "status": "failed",
            "outcome": f"failed to add the word {word} to the database"
        }

    mongo.db.words.insert_one(new_word)

    return {
        "status": "success",
        "outcome": f"successfully added the word {word['word']}"
    }


@app.route("/api/v1/single/equal/<int:equal>", methods=['GET'])
def single_equal(equal):
    word = {
        "status": "success"
    }

    try:
        word.update(get_random_doc([
            {"$match": {"$expr": {"$eq": [{"$strLenCP": "$word"}, equal]}}},
            {"$sample": {"size": 1}},
            {"$unset": "_id"},
        ]))
    except Exception:
        return {
            "status": "failed",
            "outcome": f"no words of size {equal}"
        }

    return word


@app.route("/api/v1/multiple/<int:amount>", methods=['GET'])
def multiple(amount):
    word = {
        "status": "success",
    }
    try:
        word.update(get_random_doc([
            {"$sample": {"size": amount}},
            {"$unset": "_id"}]
        ))
    except Exception:
        return {
            "status": "failed",
            "reason": "unable to retrieve words"
        }

    return word


@app.route("/api/v1/all", methods=["GET"])
def all():
    word = {"status": "success"}
    words = []

    for doc in mongo.db.words.aggregate([{"$unset": "_id"}]):
        words.append(doc["word"])

    word.update({"words_all": words})
    return word


@app.route("/api/v1/single", methods=["GET"])
def single():
    word = {"status": "success"}
    word.update(get_random_doc([
        {"$sample": {"size": 1}},
        {"$unset": "_id"}]))

    return word


@app.route("/api/v1/delete/<string:word>", methods=['DELETE'])
@jwt_required
def delete(word):
    mongo.db.words.remove({"word": f"{word}"})

    return jsonify({"status": "success"}), 200

@app.route("/")
def root():
    return redirect("/api/v1/")

@app.route("/api/v1/")
def routes():
    routes = []

    for route in app.url_map.iter_rules():
        routes.append('%s' % route)
    return {
        "routes": routes
    }

@app.errorhandler(404)
def page_not_found(e):
    return redirect("/api/v1/")

def get_random_doc(options):
    myresults = mongo.db.words.aggregate(options)

    final_return = {
        "words": []
    }

    for word in myresults:
        final_return['words'].append(word['word'])

    return final_return
