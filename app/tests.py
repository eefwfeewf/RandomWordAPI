import unittest
import requests
import json
import re
from app import config


BASE_URL = "https://randomword.bradleycarter.xyz/api/v1/"


class TestAuth(unittest.TestCase):
    def test_auth_shouldfail(self):
        token = requests.post(f"{BASE_URL}auth", json={"username": "fail","password": "fail"}).text
        self.assertFalse("jwt" in token)

    def test_auth_shouldpass(self):
        token = requests.post(f"{BASE_URL}auth",
                              json={"username": config.JWT_USER , "password": config.JWT_PASS}).text
        self.assertTrue("jwt" in token,msg="Response should of included an auth token")


class TestWordAddDelete(unittest.TestCase):
    def test_delete_word(self):
        token = requests.post(f"{BASE_URL}auth",
                              json={"username": config.JWT_USER, "password": config.JWT_PASS}).text

        token = json.dumps(token)["jwt"]

        word = "Test"

        headers = {"Authorization" : f"Bearer {token}"}

        response = requests.post(f"{BASE_URL}delete/{word}",headers=headers).text

        self.assertTrue("success" in response)

    def test_add_word(self):
        token = requests.post(f"{BASE_URL}auth",
                              json={"username": config.JWT_USER, "password": config.JWT_PASS}).text

        token = json.dumps(token)["jwt"]

        headers = {"Authorization" : f"Bearer {token}"}

        word = "Test"

        response = requests.post(f"https://randomword.bradleycarter.xyz/api/v1/delete/{word}",headers=headers).text

        self.assertTrue("success" in response)


class TestWord(unittest.TestCase):
    def test_get_word_equal(self):
        response = requests.get(f"{BASE_URL}single/equal/18").text

        l = json.loads(response)

        self.assertEqual(l['words'][0], "Telecommunications")

    def test_get_chain(self):

        response = requests.get(f"{BASE_URL}chain/2").text

        l = json.loads(response)

        a = re.findall('[A-Z][^A-Z]*', l['word_chain'])

        self.assertEqual(len(a), 2)

    def test_multiple(self):

        response = requests.get(f"{BASE_URL}multiple/5").text

        response_object = json.loads(response)

        self.assertEqual(len(response_object["words"]), 5)

    def test_multiple_less(self):

        response = requests.get(f"{BASE_URL}multiple/300/more/12").text

        response_object = json.loads(response)

        self.assertEqual(len(response_object["words"]), 300)



if __name__ == '__main__':
    unittest.main()
