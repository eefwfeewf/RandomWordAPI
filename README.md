# Random Word API



Random Word API is a Python API for dealing with random word generation.
This is the repo of the API running at https://randomword.bradleycarter.xyz/



# Install
If you would like run your own instance of the API you will need a few things

* Docker
* Docker-Compose
* Python 3.7 

A list of Python packages can be seen in the requirements.txt file.

Read the CI/CD section below to setup a CI/CD pipeline.

To deploy locally remove environment section from the docker-compose.yml file and add 'ports: "80:5000"' to the web
container section.

## Config

A config.py file MUST be placed in the app directory. It must contain three variables. An example Config
```python
import random
import string

JWT_USER = "user"
JWT_PASS = "pass"
JWT_SECRET_KEY = ''.join(random.choices(string.ascii_uppercase + string.digits, k=15))
```
#### Getting Started

```bash
git clone https://github.com/failing/RandomWordAPI.git

cd RandomWordAPI/

sudo docker-compose up --build -d
```

The mongo database will be initially empty. You may seed it using the words.json file and mongoimport.

### Without Docker

```bash
git clone https://github.com/failing/RandomWordAPI.git

cd RandomWordAPI/

virtualenv venv

source venv/bin/activate 

pip3 install -r app/requirements.txt

python3 wsgi.py
```

## Usage

### Single Word
Will return a single word
```
/api/v1/single
```

Expected Output
```json
  {
      "status":"success",
       "words":["Kit"]
  }
```

### Add Word
Add a word to the database. A user must be authenticated user the /auth endpoint and a Authorization token must be
supplied in the request.

```
/api/v1/add
```

Expected output

```json
    {
        "status": "success",
        "outcome": "successfully added the word Testing"
    }
```

### All
Get all words as a list. Note the order of the words will be different each time.


```/api/v1/all```

Expected output
```json

   { 
      "status":"success", 
      "words_all":["Devil","Delle","Mobil",......]
    }
```

### Chain
Will return n amount of words "chained" together.

```
/api/v1/chain/{n}
```

Expected output
```json

  {
    "status":"success",
    "word_chain":"AdhdNomIllusionsStrippingLettuce"
} 

```


You can see all available endpoints via ```/api/v1/``` 


